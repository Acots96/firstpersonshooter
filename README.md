## PEC2 


Este es un juego de acción en primera persona (FPS), compuesto por dos niveles (zona exterior montañosa y zona cubierta):
 - Nivel 1: zona inicial del juego, descubierta, en la que el jugador encontrará una serie de enemigos y deberá llegar hasta la toma de energía para activarla (mediante un botón). Esta proporcionará energía al siguiente botón, el cual permitirá que el jugador pueda activar la plataforma horizontal que le permitirá cruzar el precipicio. Una vez ha llegado al otro lado del precipicio, de nuevo encontrará más enemigos pero uno de ellos contiene la llave que le permitirá acceder al nivel 2.
 - Nivel 2: zona cubierta del juego, en la que el jugador deberá deberá encontrar y activar los dos botones vigilados por enemigos que le permitirán abrir la puerta, que solo se mantendrá abierta durante un breve periodo de tiempo tras haber activado ambos botones. Tras sortear esa puerta el jugador se encontrará en la parte central del complejo, con el objetivo de sortear más enemigos para poder llegar hasta el ascensor que lo conducirá hasta la última parte, donde se topará con el último grupo de enemigos custodiando el botón que el jugador debe pulsar para ganar el juego.
 
Desde el principio el jugador poseerá dos armas de fuego y granadas. Las teclas que puede usar son:
 - Moverse, saltar y girar: WASD, Espacio y ratón.
 - Disparar: botón izquierdo del ratón (mantener pulsado para disparar varias veces seguidas con el rifle o para aguantar la granada antes de lanzarla).
 - Apuntar: botón derecho del ratón (solo para el rifle).
 - Recargar: R.
 - Interactuar con botones: E.
 - Agacharse: mantener LeftCtrl.

### Terreno:
El terreno está formado con la herramienta Terrain de Unity. Ha sido elevado hasta cierta altura con una parte hundida en el centro, para poder simular un precipicio con agua que lo separe en dos partes:
 - En la primera parte se encuentra un complejo de instalaciones militares cuya función es únicamente decorativa, ya que no se puede entrar. También hay un conjunto de cajas y bidones puestos aleatoriamente para que el jugador pueda tratar de camuflarse de los enemigos. Finalmente también están los botones con los que interactuará y una plataforma sostenida por una serie de pilares que conectan ambos lados del precipicio.
 - La segunda parte está dividida en dos:
   1. La primera subparte es muy parecida a la primera parte del juego, ya que también es exterior y contiene cajas y bidones colocados aleatoriamente.
   2. La segunda subparte es una instalación en la que el jugador deberá entrar. Está formada por una planta inferior con 3 ramificaciones (dos pasillos a los lados y una más ancha en el centro), una parte superior con la misma forma que la parte central inferior y una columna con una plataforma que conecta ambas partes inferior y superior.
 
### HUD:
El HUD proporciona al jugador la información necesaria: 
 - En la parte inferior están la vida y el escudo (izquierda), la cantidad de llaves que posee el jugador (centro), y la munición del arma que el jugador está usando en ese momento (derecha) que se muestra como "balasCargadas/balasRestantes" (pueden ser balas o bombas). Todos estos números del HUD inferior tienen al lado un pequeño icono para dar información sobre qué es ese número: la vida y el escudo tienen una cruz roja y un escudo morado respectivamente, el número de llaves tiene una llave cyan y la munición tiene un pequeño icono que indica el tipo de arma, el cual cambia cuando el jugador cambia de arma.
 - En la parte central está la mirilla, formada por 4 rectángulos blancos (2 verticales y 2 horizontales) separados entre ellos para dejar un espacio en el centro.
 - En la parte inferior (ligeramente encima de los números e iconos) tiene una barra semitransparente con texto que da información al jugador sobre qué debe hacer al llegar a una nueva area (aparece durante un tiempo determinado), y un texto que aparece en la parte central con información que dan algunos elementos como puertas que no se pueden abrir sin una llave, entre otras cosas.
 - También contiene un panel semitransparente rojo que se activa durante un instante de tiempo al recibir daño.
 - Finalmente las pantallas de GameOver y Win contienen un panel negro y la información sobre qué hacer para seguir jugando.

### Armas:
En total en el juego hay 3 tipos de armas:
 - Rifle: Arma automática con la que el jugador puede disparar múltiples veces sin soltar el botón de disparo. Es un arma con daño medio, pensada para distancias más largas. Ambas armas de fuego tienen cierto retroceso, lo que simula el típico "empujón" del arma hacia arriba y ligeramente hacia atrás, y por lo tanto provoca que, en el caso del rifle, los siguientes disparos también vayan hacia arriba. También ambas armas utilizan el Raycast desde el centro del jugador para comprobar el objeto impactado por el disparo.
 - Pistola: Arma no automática con la que el jugador puede disparar una sola vez al pulsar el botón de disparo. Posee un gran daño y más precisión que el rifle, ideal para distancias cortas. En las dos armas de fuego, la precisión viene determinada por un factor entre 0 y 1 que provoca una ligera desviación aleatoria: cuanto más cerca del 1 menor es el número de aleatoriedad, por lo que el disparo es más preciso.
 - Granada: Arma arrojadiza que explota al tocar algún objeto. Causa un daño máximo, dependiendo de la distancia entre el centro de la explosión y el objetivo dañado. Ideal para provocar daño en area. El jugador puede decidir cuando la tira tras haber pulsado el botón de disparo, siempre que lo mantenga pulsado, hasta un límite de 3 segundos, entonces tira la granada.

### Vida/Escudo:
El jugador posee cierta cantidad de vida y escudo para sobrevivir. 100 puntos en ambos casos, pero el escudo hace que la vida baje más despacio al recibir daño, por lo que la mayor parte del daño es "absorvida" por el escudo y el daño solo afecta en un 20% a la vida. Cuando los puntos de escudo llegan a 0 la vida desciende normalmente al recibir daño. Tras morir, el jugador reaparece con 75 puntos de vida y 50 de escudo.

### Botones:
A lo largo del juego hay distintos botones cuya función es activar algún otro elemento, como por ejemplo una plataforma móvil, un ascensor u otro botón. Este elemento tiene tres estados: desconectado, desactivado y activado. Lo normal es que un botón esté activado o desactivado, pero también los hay que pueden estar desactivados (unplugged) de inicio y sea necesario conectarlos mediante algún otro botón para luego poder activarlos. Todos tienen la misma estructura, que consta de una caja negra alta como el jugador y dos cilindros (rojo y verde) que se activan según el botón esté activado o desactivado, con los colores verde y rojo respectivamente. Contienen un BoxCollider usado como "trigger" para poder interactuar con el jugador.

### Plataforma/Ascensor:
En todo el juego hay 2 plataformas móviles: la primera se mueve horizontalmente para transportar al jugador de un lado del precipicio a otro. La segunda se mueve verticalmente dentro de una columna en las instalaciones. Ambos contienen un BoxCollider usado como "trigger" que se encarga de comprobar (con un LayerMask) los objetos que entran y hacerlos "hijos" (solo al GameObject parent) de la plataforma para que, al moverse, el jugador se mueva al mismo tiempo y no se "resbale". Para controlar que solo modifican el parent del GO correcto utiliza una LayerMask con los layers que debe tener en cuenta. Para evitar que el jugador se pueda caer, las plataformas pueden bloquear el movimiento del jugador mientras se están moviendo.

### Puertas:
A lo largo de la parte edificada el jugador encontrará varias puertas, las cuales se abren automáticamente al detectarlo (mediante un BoxCollider trigger), pero hay dos que requieren distintas acciones. Una de ellas requiere una llave que entregará uno de los enemigos al morir, mientras que la otra funciona solo al activar dos botones y solo durante un tiempo determinado, por lo que si el jugador no logra llegar antes de que se acabe el tiempo no podrá cruzarla. Cada puerta está formada por sus dos partes, que se mueven horizontalmente hacia el exterior cuando el jugador está cerca. Cuando está lo suficientemente lejos vuelven a moverse hacia el interior.

### Enemigos:
 - El jugador hallará enemigos en todas las partes del juego. Están diseñados para deambular libremente por la zona a la que están asignados (mediante una serie de puntos entre los que escogen aleatoriamente para moverse) y para estar atentos al jugador, por lo que si está lo suficientemente cerca podrán verle y empezar a dispararle, o si por lo contrario no pueden verle pero se está moviendo dentro de su rango de oído, entonces darán una vuelta completa sobre sí mismos para "averiguar" qué era ese ruído:
   - Para detectar visualmente al jugador, el enemigo lanza una serie de Raycast (SphereCast concretamente, más detalles en el código de EnemyController) a lo largo de 120º que simulan su campo de visión (suponiendo 0º el transform.forward, los ángulos van de -60º hasta +60º, y luego se transforman a WorldSpace), uno cada frame ya que todos a la vez sería un gasto de recursos importante e innecesario. 
   - Para "escucharlo" comprueban si el jugador se está moviendo dentro de un rango (consiste en preguntar al FirstPersonController si la Speed es 1 o superior) y entonces ejecuta una corutina que le hace dar una vuelta de 360º en 4 segundos o hasta que vea al jugador. 
 - En el caso de que el enemigo reciba disparos del jugador, se girará y correrá hacia él, disparándole. Finalmente, si pierde de vista al jugador correrá hacia él durante unos segundos y, si sigue sin verle, dará una vuelta de 360º y seguirá con su "Wander". 
 - Para disparar únicamente espera un tiempo aleatorio (entre 0.5s y 1.5s) y entonces ejecuta los métodos correspondientes del arma. Si tiene un arma automática entonces dispara un número aleatorio de veces (entre 1 y 5), si por el contrario tiene una pistola entonces solo hace un disparo cada vez.
 - Hay dos tipos de enemigos en el juego, los que llevan pistola y los que llevan rifle. Todos tienen un item que dropean cuando mueren, que siempre es un número aleatorio de munición del arma que llevan, pero en un caso concreto es la llave de una puerta. Finalmente, cuando un enemigo muere utiliza una instancia propia del material que tienen sus MeshRenderers para modificar el componente alpha del color progresivamente, y así simular un "fade" hasta que pasan a ser totalmente transparentes, en ese momento desaparece el enemigo.

### Items:
Por todo el terreno hay varios items para que el jugador pueda recogerlos, que consisten en vida, escudo y granadas, mientras que los items de munición (y llave en una ocasión) los dejan caer los enemigos al morir. Para recibir su bonificación el jugador solo debe pasar por encima de ellos. Los items de munición de arma de fuego no contienen la misma cantidad de balas siempre, sino que se elige aleatoriamente un número que esté entre el 70% de la munición y el 100%, para simular que una parte la ha gastado el enemigo que la deja caer.

### Sonidos:
La mayoría de acciones y efectos del juego están sonorizados, y algunos de ellos utilizan el "Spatial Blend" para hacer que la distancia afecte al volumen y poder simular sonido en 3D. No contiene música debido a que la mayoría de juegos actuales de acción FPS no la tienen. Los sonidos implementados son:
  - Pasos y salto del jugador, ya implementados con el script "FirstPersonController" que viene en los "Standard Assets".
  - Recibir daño y morir: hay 4 sonidos de recibir daño que se ejecutan aleatoriamente para que no sea siempre el mismo sonido, y 1 de morir. Estos 5 sonidos son usados tanto por el jugador como por los enemigos.
  - Recoger items: se utilizan dos sonidos distintos para recoger los items de vida y escudo, uno propio para las llaves y el mismo sonido para recoger los 3 tipos de items de munición.
  - Botón: siempre usa el mismo sonido.
  - Puertas: tienen un sonido en bucle que se reproduce siempre que se estén abriendo o cerrando.
  - Plataforma/Ascensor: Tienen un sonido distinto cada uno, que suena en bucle mientras se esté moviendo.
  - Timer: sonido que emula una cuenta atrás para la puerta que tarda X segundos en desconectarse.
  - Armas listas: cada vez que el jugador cambia de arma de fuego se reproduce el característico sonido de dejar lista un arma para ser disparada.
  - Disparos: También hay dos sonidos distintos para las dos armas de fuego. La pistola tiene un sonido ligeramente más largo, que se reproduce una vez por disparo, mientras que el rifle tiene un sonido más corto, que también se reproduce una vez por disparo pero simula un bucle cuando el jugador mantiene el botón de disparo pulsado.
  - Granada: la granada tiene distintos sonidos: el primer sonido es el de abrirla, que se reproduce al pulsar el botón de disparo. En ese mismo instante se empieza a reproducir el típico sonido de mecha quemándose. En último lugar se reproduce el sonido de explosión cuando la granada toca algún objeto sólido o un enemigo. 
  - Final: al finalizar el juego suena una breve melodía de victoria.

### Checkpoints:
A lo largo del juego hay 3 checkpoints en los que el jugador reaparecerá al morir siempre que haya logrado llegar hasta ellos antes. Tienen un BoxCollider que hace de trigger y registra cuando el jugador llega al checkpoint.
  1. El primero es el inicial del juego, donde aparece el jugador por defecto al empezar y reaparecerá siempre que muera y no haya llegado al siguiente.
  2. El segundo se encuentra justo al bajar de la plataforma al otro lado del precipicio, y hará que el jugador reaparezca en la plataforma en su posición inicial (deberá pulsar el botón para cruzar el precipicio de nuevo).
  3. El tercero y último se encuentra en el edificio, justo tras la puerta de entrada.

### Efectos de partículas:
No es algo que destaque especialmente, pero sí hay algunos:
  - Armas: las armas de fuego tienen un efecto de luz pequeño en la punta del cañón al disparar, con forma de "X", que simula el fuego del disparo, y además un efecto de pequeñas partículas que saltan en el lugar de impacto de la bala cuando choca con un objeto sólido. Finalmente la granada provoca un efecto de explosión.
  - Actores: todos los actores (enemigos y jugador) tienen un pequeño efecto de partículas cuando reciben un disparo, muy parecido al que provoca un impacto en un objeto sólido, pero este efecto tiene un material de color rojo, para simular sangre expulsada por el impacto.

### Scripts:
 - GameManager: script principal que controla la información mostrada por pantalla, la muerte del jugador (junto con las reapariciones) y al terminar el juego.
 
 - Weapon: script del que heredan todas las armas y controla tanto la información básica de un arma (munición cargada, munición restante) como los métodos abstractos de disparo (pulsar, mantener pulsado, soltar) que deben implementar las subclases. Además también tiene un método de recarga, el cual es "virtual" porque no todas las armas funcionan con la misma lógica de recarga (granada), por lo que no obliga a implementarlo. También gestiona el sonido de tener el arma lista.
 
   - FireWeaponController: script que implementan todas las armas de fuego (pistola y rifle), que hereda de Weapon. Gestiona la lógica de disparo y recarga (implementación de los métodos abstractos y virtual), el efecto de retroceso y el Raycast al disparar a alguien (y su consiguiente aplicación de daño si se trata de un Actor).
  
   - ThrowableWeaponController: script que implementan todas las armas lanzables (granada), que también hereda de Weapon. Al igual que el anterior, también debe implementar los métodos de disparo, con la diferencia de que este arma puede estar sujeta al mantener pulsado el botón, por lo que se implementa un "contador" que provoca su lanzamiento al llegar a 0. Dado que este arma no funciona con Raycast sino que es un objeto sólido (a diferencia de las de fuego, que no usan objetos de balas), el script contiene el prefab que instancia cada vez que el jugador lanza una granada, solo en el caso de que tenga granadas de reserva (sino se instancia una al recoger los items de granada).
  
   - Grenade: script que simula el funcionamiento de la granada al chocar contra el suelo, generando el efecto de partículas, reproduciendo el sonido de explosión (y el de la mecha gastándose desde qe el jugador la activa) y aplicando daño a los actores que encuentra, por lo que busca a todos los Actors que estén dentro del radio de explosión y les aplica un % de daño según la distancia a la que estén de la explosión (daño=(1-distancia/radioMaximo)*dañoMaximo).
  
 - Actor: script del que heredan todos los actores, ya sean enemigos o el mismo jugador. Gestiona los puntos de vida y los métodos que deben/pueden implementar las subclases (TakeDamage para recibir daño). También contiene los sonidos de recibir daño y morir.
 
   - PECController: script que hereda de Actor y controla las funciones del jugador relativas a la PEC: resetear al player en un checkpoint cuando muere, controlar las acciones que realiza (interactuar con botones, recargar o cambiar de arma, agacharse, apuntar y disparar), gestionar el daño teniendo en cuenta el escudo, avisar a GameManager para actualizar la UI y gestionar la recogida de items. Como añadido extra, este script posee una variable modificable desde el inspector de Unity (IsInmortal) para poder jugar sin recibir daño (muere igualmente si cae por el precipicio).
  
     - FirstPersonController: script que controla el movimiento del jugador y de la cámara. Está en el mismo GameObject que PECController.
   
   - EnemyController: script que también hereda de Actor e implementa los mismos métodos de disparo pero no de recarga, ya que el enemigo no tiene un límite de balas. También gestiona la IA, la lógica de disparo y la muerte con el efecto de "fade", todo explicado previamente en el apartado "Enemigos".
  
 - ActivableElement: script del que heredan todos los elementos activables (botones y puertas), y que contiene los métodos abstractos de conexión y activación que deben implementar las subclases, para definir lo que les ocurre al conectarse (Plug) y al activarse (Activate). También contienen los métodos abstractos que devuelven un booleano indicando si el activable está conectado y si está activado (también deben ser implementados por las subclases). GameManager también hereda de este script para poder finalizar la partida al pulsar el botón de la parte superior del edificio.
 
   - ButtonController: script que hereda de ActivableElement y se encarga de activar otros elementos como pueden ser puertas, plataformas u otros botones. Gestiona la lógica de activar y desactivar los cilindros verde y rojo que indican si está activado o no. También reproduce el sonido correspondiente al ser pulsado.
  
   - DoorController: script que hereda de ActivableElement y se encarga de gestionar la lógica y el movimiento de las puertas. También reproduce el sonido mientras se está abriendo o cerrando, y lo para cuando la puerta se ha abierto completamente o está cerrada.
  
 - PlatformController: script que controla el funcionamiento de las plataformas móviles (plataforma horizontal y ascensor). También se encarga de reproducir en bucle el sonido de cada plataforma y controlar cuando debe pararlo. Para mover la plataforma utiliza dos Vector3 (Pos1 y Pos2) y una corutina que mueve a cierta velocidad la plataforma de un punto a otro.
 
 - Item: script que guarda la información de un item que puede recoger el jugador, ya sea munición, una granada, vida, escudo o una llave. También reproduce el sonido de coger el item, cosa que hace que este no desaparezca al instante, sino cuando el sonido ha acabado.
 
 - EventThrower: script auxiliar cuya función es lanzar un UnityEvent en el momento en el que un GameObject concreto entra en su BoxCollider. Tiene 3 formas de comprobar un GameObject: por una LayerMask (para tener en cuenta ciertos layers), por una lista de tags y/o por una lista de GOs concretos (para seleccionar manualmente qué GOs activan el trigger del Collider).
 
*(Ver la implementación del código y los comentarios para más detalles. Todos los elementos añadidos están en Assets/Pec/)*
 
 
Video link: https://youtu.be/1k2vryu61MM