﻿using System.Collections;
using UnityEngine;

public class DoorController : ActivableElement {

    [SerializeField] private bool IsPluggedIn, NeedsKey;
    [SerializeField] private int ButtonsActiveNeeded;

    [SerializeField] private Transform LeftDoor, RightDoor;
    [SerializeField] private Vector3 OpenLeftLocalPos, OpenRightLocalPos;
    [SerializeField] private float DoorActionTime;

    [SerializeField] private AudioSource MovingSound;

    [Header("TimeDoor")]
    [SerializeField] private bool IsTimeDoor;
    [SerializeField] private float TimeToUnplug;
    [SerializeField] private ButtonController[] Buttons;
    [SerializeField] private GameObject[] EnabledLights, DisabledLights;
    [SerializeField] private AudioSource CountdownSound;

    private bool isPlayerClose;
    private Vector3 ClosedLeftLocalPos, ClosedRightLocalPos;
    private int buttonsActivated;


    private void Awake() {
        isPlayerClose = false;
        ClosedLeftLocalPos = LeftDoor.localPosition;
        ClosedRightLocalPos = RightDoor.localPosition;
        StartCoroutine(AutomaticDoorEffect(LeftDoor, ClosedLeftLocalPos, OpenLeftLocalPos));
        StartCoroutine(AutomaticDoorEffect(RightDoor, ClosedRightLocalPos, OpenRightLocalPos));
    }

    
    public override bool IsPlugged() => IsPluggedIn;
    public override bool IsActivated() => IsPluggedIn;


    public override void Plug() {
        IsPluggedIn = true;
    }

    public override void Activate() {
        buttonsActivated++;
        if (IsTimeDoor) {
            EnabledLights[buttonsActivated - 1].SetActive(true);
            DisabledLights[buttonsActivated - 1].SetActive(false);
            if (buttonsActivated == ButtonsActiveNeeded) {
                IsPluggedIn = true;
                if (CountdownSound)
                    CountdownSound.Play();
                Invoke("WaitToClose", TimeToUnplug);
            }
        }
    }

    private void WaitToClose() {
        buttonsActivated = 0;
        IsPluggedIn = isPlayerClose = false;
        foreach (ButtonController btn in Buttons)
            btn.EnableButton(false);
        foreach (GameObject light in EnabledLights)
            light.SetActive(false);
        foreach (GameObject light in DisabledLights)
            light.SetActive(true);
        if (CountdownSound)
            CountdownSound.Stop();
    }


    private IEnumerator AutomaticDoorEffect(Transform door, Vector3 close, Vector3 open) {
        float multiplier;   // -1=close, 1=open
        float progress = 0;
        //al convertir el bool en un int conseguimos que la puerta siempre "quiera"
        //dirigirse hacia un extremo u otro, por lo que solo hay que cambiar ese int
        //para que la puerta se abra o se cierre
        while (Application.isPlaying) {
            multiplier = isPlayerClose ? 1 : -1;
            progress += Time.deltaTime / DoorActionTime * multiplier;
            progress = Mathf.Max(0, Mathf.Min(1, progress));
            door.localPosition = Vector3.Lerp(close, open, progress);
            if (progress <= 0 || progress >= 1) {
                if (MovingSound && MovingSound.isPlaying)
                    MovingSound.Stop();
            }
            yield return null;
        }
    }


    private void OnTriggerEnter(Collider other) {
        int layer = other.gameObject.layer;
        if (GameManager.CheckPlayerMask(layer)) {
            if (!IsPlugged()) {
                isPlayerClose = false;
                GameManager.ShowNotPluggedInfo();
            } else {
                //puede abrirse porque no necesita nada
                if (!NeedsKey && ButtonsActiveNeeded <= 0) {
                    isPlayerClose = true;
                    GameManager.ShowEmpty();
                    if (MovingSound)
                        MovingSound.Play();
                //necesita llave y o botones para abrirse, y comprueba si se cumplen los requisitos
                } else {
                    bool hasKey = !NeedsKey;
                    if (NeedsKey) {
                        NeedsKey = !GameManager.CheckPlayerHasKey();
                        if (NeedsKey) {
                            GameManager.ShowNeedKeyInfo();
                        } else {
                            //isPlayerClose = true;
                            //GameManager.ShowEmpty();
                            hasKey = true;
                        }
                    }
                    isPlayerClose = hasKey && buttonsActivated == ButtonsActiveNeeded;
                    if (isPlayerClose && MovingSound)
                        MovingSound.Play();
                }
            }
        }
    }

    private void OnTriggerExit(Collider other) {
        int layer = other.gameObject.layer;
        if (GameManager.CheckPlayerMask(layer)) {
            isPlayerClose = false;
            if (MovingSound)
                MovingSound.Play();
            GameManager.ShowEmpty();
        }
    }
}
