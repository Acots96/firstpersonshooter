﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Actor : MonoBehaviour {

    [SerializeField] protected float Life;

    [SerializeField] protected AudioSource[] HitSounds;
    [SerializeField] protected AudioSource DieSound;

    public bool IsPlayer { get; protected set; }

    public abstract void TakeDamage(float dmg);
    public virtual void Fire(float amount) { }
    public virtual void AddAmmo(float amount) { }
}
