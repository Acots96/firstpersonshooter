﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowableWeaponController : Weapon {

    [SerializeField] private float HoldTime;
    [SerializeField] private float ThrowForce;
    [SerializeField] private GameObject ThrowablePrefab;

    private bool opened;
    private float heldTime;
    private Vector3 startLocalPos;
    private GameObject throwableGO;


    private void Awake() {
        heldTime = 0;
        loadedBullets = 1;
        cartridgesBullets = Amount - 1;
        //startLocalPos = transform.GetChild(0).localPosition;
        startLocalPos = ThrowablePrefab.transform.localPosition;

        throwableGO = Instantiate(ThrowablePrefab, transform);
        throwableGO.transform.localPosition = startLocalPos;
    }


    public override void ButtonDown() {
        if (opened)
            return;
        if (Amount > 0) {
            opened = true;
            throwableGO.GetComponent<Grenade>().Open();
            ButtonHeld();
        }
    }

    public override void ButtonHeld() {
        if (Amount <= 0)
            return;
        heldTime += Time.deltaTime;
        if (heldTime >= HoldTime)
            ButtonUp();
    }

    public override void ButtonUp() {
        if (!opened)
            return;
        if (Amount <= 0)
            return;
        opened = false;
        heldTime = 0;
        ThrowIt();
    }

    public override void AddAmmo(int amount) {
        if (Amount <= 0) { //si no tiene se instancia una
            throwableGO = Instantiate(ThrowablePrefab, transform);
            throwableGO.transform.localPosition = startLocalPos;
            PlayReadySound();
        }
        Amount += amount;
        loadedBullets = 1;
        cartridgesBullets = Amount - loadedBullets;
    }


    private void ThrowIt() {
        Transform throwable = throwableGO.transform;
        throwable.parent = null;
        Vector3 direction = transform.forward + Vector3.up * 0.5f;
        Rigidbody rb = throwable.gameObject.GetComponent<Rigidbody>();
        rb.useGravity = true;
        rb.AddForce(direction * ThrowForce, ForceMode.Impulse);
        throwable.gameObject.AddComponent<CapsuleCollider>();
        throwableGO = null;
        //
        Amount--;
        if (Amount > 0) { //si todavia tiene se instancia otra
            throwableGO = Instantiate(ThrowablePrefab, transform);
            throwableGO.transform.localPosition = startLocalPos;
            loadedBullets = 1;
            PlayReadySound();
        } else {
            loadedBullets = 0;
        }
        cartridgesBullets = Amount - loadedBullets;
        owner.Fire(1);
    }

}
