﻿using UnityEngine;

public class FireWeaponController : Weapon {

    public bool IsAutomatic;
    [SerializeField] private float FireRate;
    public int BulletsPerCartridge;
    [SerializeField, Range(0, 1)] private float Accuracy;
    [SerializeField] private Transform ShootingCenter;

    [SerializeField] private ParticleSystem FireEffect, BulletImpactEffect, EnemyImpactEffect;

    [SerializeField] private AudioSource FireSound;

    private Transform tr;
    private float rateTime;
    private Vector3 startLocalPos, shootingLocalPos;
    private Quaternion startLocalRot;


    private void Awake() {
        tr = transform;
        rateTime = 0;
        shootingLocalPos = tr.localPosition;
        shootingLocalPos.x = shootingLocalPos.y = 0;
        startLocalPos = tr.localPosition;
        startLocalRot = tr.localRotation;
        //
        cartridgesBullets = Amount - BulletsPerCartridge;
        loadedBullets = BulletsPerCartridge;
    }

    private void Update() {
        if (rateTime < FireRate)
            rateTime += Time.deltaTime;
        //para que el arma vuelva a su posicion central
        tr.localRotation = Quaternion.Slerp(tr.localRotation, startLocalRot, Time.deltaTime * 10);
        tr.localPosition = Vector3.Lerp(tr.localPosition, startLocalPos, Time.deltaTime * 10);
    }


    public override void ButtonDown() {
        if (rateTime < FireRate || loadedBullets == 0)
            return;
        if (FireEffect)
            FireEffect.Play();
        Fire();
    }

    public override void ButtonHeld() {
        if (rateTime < FireRate || !IsAutomatic || loadedBullets == 0) {
            if (loadedBullets == 0 && FireEffect)
                FireEffect.Stop();
            return;
        }
        Fire();
    }

    public override void ButtonUp() {
        if (FireEffect)
            FireEffect.Stop();
    }

    public override void AddAmmo(int amount) {
        Amount += amount;
        cartridgesBullets = Amount;
    }


    private void Fire() {
        if (loadedBullets == 0) {
            return;
        }
        loadedBullets--;
        rateTime = 0;
        //
        float acc = 1 - Accuracy;
        Vector3 center = ShootingCenter.position;
        center.y = tr.position.y;
        //desviacion aleatoria segun la precision (Accuracy) para que no sea siempre recto
        Vector3 direction = tr.forward + Random.Range(-0.025f, 0.025f) * acc * tr.right + Random.Range(0.05f, 0.15f) * acc * tr.up;
        direction = direction.normalized;
        Debug.DrawLine(center, center + direction * 100 * tr.localScale.x, Color.cyan);
        Ray ray = new Ray(center, direction);
        if (Physics.Raycast(ray, out RaycastHit hit, 100f * tr.localScale.x, ~GameManager.GetWeaponIgnoreMask(), QueryTriggerInteraction.Ignore)) {
            Actor a = hit.transform.GetComponent<Actor>();
            if (a) { //daño y efecto de sangre expulsada
                float dmg = Damage;
                if (GameManager.CheckActorHeadMask(hit.collider.gameObject.layer))
                    dmg *= 2f;
                a.TakeDamage(dmg);
                Quaternion rot = Quaternion.Euler(hit.normal) * Quaternion.Euler(-90, 0, 0);
                GameObject impact = Instantiate(EnemyImpactEffect.gameObject, hit.point, rot);
                impact.GetComponent<ParticleSystem>().Play();
            } else { //efecto de golpe de bala en la pared
                Quaternion rot = Quaternion.Euler(hit.normal) * Quaternion.Euler(-90, 0, 0);
                GameObject impact = Instantiate(BulletImpactEffect.gameObject, hit.point, rot);
                impact.GetComponent<ParticleSystem>().Play();
            }
        }
        //simulacion del retroceso que empuja el arma hacia atras (rotacion hacia arriba)
        tr.Rotate(new Vector3(-4, 0), Space.Self);
        Amount = loadedBullets + cartridgesBullets;
        owner.Fire(1);
        if (FireSound)
            FireSound.Play();
    }

    public bool CanReload() {
        return Amount > 0 && loadedBullets < BulletsPerCartridge;
    }
    public override void Reload() {
        if (loadedBullets == BulletsPerCartridge)
            return;
        if (cartridgesBullets == 0) {
            return;
        }
        PlayReadySound();
        float askedBullets = BulletsPerCartridge - loadedBullets;
        float actualCartridgesBullets = Mathf.Max(0, cartridgesBullets - askedBullets);
        loadedBullets += cartridgesBullets - actualCartridgesBullets;
        cartridgesBullets = actualCartridgesBullets;
    }


    private void OnEnable() {
        FireEffect.gameObject.SetActive(true);
        FireEffect.Stop();
    }
    private void OnDisable() {
        FireEffect.Stop();
        FireEffect.gameObject.SetActive(false);
    }

}
