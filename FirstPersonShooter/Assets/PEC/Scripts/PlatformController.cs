﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : ActivableElement {

    [SerializeField] private bool LockPlayerUntilFinish;
    [SerializeField] private float Speed;
    [SerializeField] private Vector3 Pos1, Pos2;

    [SerializeField] private LayerMask NotIgnoreMask;

    [SerializeField] private AudioSource MovingSound;

    private Transform tr;
    private bool isPos1, isActivated;


    private void Awake() {
        tr = transform;
        isPos1 = Vector3.Distance(tr.position, Pos1) < Vector3.Distance(tr.position, Pos2);
        isActivated = false;
    }


    public override void Plug() {
        // always plugged
    }

    public override void Activate() {
        if (isActivated)
            return;
        isActivated = true;
        if (MovingSound)
            MovingSound.Play();
        StartCoroutine(MoveIt());
    }

    public override bool IsPlugged() => true;
    public override bool IsActivated() => isActivated;


    public void ResetPlatform() {
        tr.position = Pos1;
        GetComponentInChildren<ButtonController>().EnableButton(false);
    }


    //corutina para mover una plataforma de una posicion a la otra
    private IEnumerator MoveIt() {
        PECController pc = GetComponentInChildren<PECController>();
        if (LockPlayerUntilFinish && pc)
            pc.CanMove = false;
        Vector3 target = isPos1 ? Pos2 : Pos1;
        Vector3 direction = (target - tr.position).normalized;
        while (Vector3.Distance(tr.position, target) > 1f) {
            tr.position += Time.deltaTime * direction * Speed;
            yield return null;
        }
        isActivated = false;
        if (MovingSound)
            MovingSound.Stop();
        if (LockPlayerUntilFinish && pc)
            pc.CanMove = true;
    }


    private void OnTriggerEnter(Collider other) {
        int layer = other.gameObject.layer;
        if (NotIgnoreMask.value == (NotIgnoreMask.value | (1 << layer)))
            other.transform.SetParent(tr);
    }

    private void OnTriggerExit(Collider other) {
        int layer = other.gameObject.layer;
        if (NotIgnoreMask.value == (NotIgnoreMask.value | (1 << layer)))
            other.transform.SetParent(null);
    }
}
