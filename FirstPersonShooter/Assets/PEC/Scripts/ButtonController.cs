﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ButtonController : ActivableElement {

    [SerializeField] private ActivableElement Activable;
    [SerializeField] private GameObject EnabledBtn, DisabledBtn;
    [SerializeField] private bool IsPluggedIn, IsEnabled, OneTimeButton = true;

    [SerializeField] private AudioSource PushButtonSound;


    private void Awake() {
        if (IsPlugged())
            EnableButton(IsEnabled);
    }


    public override void Plug() {
        IsPluggedIn = true;
    }

    public override void Activate() {
        if (!IsPlugged())
            return;
        if (EnabledBtn.activeSelf || Activable == null)
            return;
        EnableButton(true);
        if (Activable.GetType() == typeof(ButtonController)) {
            Activable.Plug();
        } else {
            Activable.Activate();
            if (!OneTimeButton)
                StartCoroutine(Wait());
        }
    }


    public override bool IsPlugged() => IsPluggedIn;
    public override bool IsActivated() => EnabledBtn.activeSelf;


    public void EnableButton(bool enable) {
        if (enable) {
            if (PushButtonSound)
                PushButtonSound.Play();
            GameManager.ShowEmpty();
        }
        DisabledBtn.SetActive(!enable);
        EnabledBtn.SetActive(enable);
    }


    private IEnumerator Wait() {
        yield return new WaitWhile(() => Activable.IsActivated());
        EnableButton(false);
    }

}
