﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour {

    [SerializeField] private float ExplosionRadius, ExplosionForce;
    [SerializeField] private GameObject ExplosionEffect;

    [SerializeField] private AudioSource OpenedSound, FuseSound, ExplosionSound;
    

    public void Open() {
        if (OpenedSound)
            OpenedSound.Play();
        if (FuseSound)
            FuseSound.Play();
    }


    private void OnCollisionEnter(Collision collision) {
        //para que no explote justo al ser lanzada por tocar al jugador
        if (GameManager.CheckPlayerMask(collision.collider.gameObject.layer))  
            return;
        if (FuseSound)
            FuseSound.Stop();
        Collider[] hitColliders = 
            Physics.OverlapSphere(transform.position, ExplosionRadius, GameManager.GetActorsMask());
        Vector3 pos = transform.position;
        //
        List<Actor> alreadyhit = new List<Actor>();
        foreach (Collider c in hitColliders) {
            Actor a = c.gameObject.GetComponentInParent<Actor>();
            //como el GO de enemigo tiene varios colliders en sus GOs hijos, solo se comprueba
            //a traves del script Actor, para golpear una unica vez a cada Actor
            if (a && !alreadyhit.Contains(a)) {
                float dmgPct = (1 - Vector3.Distance(pos, a.transform.position) / ExplosionRadius) * ExplosionForce;
                a.TakeDamage(dmgPct);
                alreadyhit.Add(a);
            }
        }
        GameObject go = Instantiate(ExplosionEffect, transform.position, Quaternion.identity);
        //poner el sonido de explosion en el efecto de particulas generado
        //para que pueda sonar sin necesidad de mantener el GameObject de la granada activo
        AudioSource aS = go.AddComponent<AudioSource>();
        aS.clip = ExplosionSound.clip;
        aS.Play();
        Destroy(gameObject);
    }

}
