﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventThrower : MonoBehaviour {

    [SerializeField] private bool OneTimeEvent;
    [SerializeField] private List<GameObject> TriggerGameObjects;
    [SerializeField] private LayerMask TriggerLayersMask;
    [SerializeField] private List<string> TriggerTags;

    [SerializeField] private UnityEvent OnTriggerEvent;


    public void ThrowEvent() {
        OnTriggerEvent?.Invoke();
        //desactivarlo si solo debe ejecutarse una vez
        if (OneTimeEvent)
            GetComponent<Collider>().enabled = false;
    }


    private void OnTriggerEnter(Collider other) {
        GameObject go = other.gameObject;
        int layer = go.layer;
        string tag = go.tag;

        //si esta en la lista de GOs 
        //o tiene uno de los layers de la LayerMask
        //o su tag esta en la lista de tags, se lanza el evento
        if (TriggerGameObjects.Contains(go) 
            || GameManager.CheckMask(layer, TriggerLayersMask) 
            || TriggerTags.Contains(tag)) {
            OnTriggerEvent?.Invoke();
            //desactivarlo si solo debe ejecutarse una vez
            if (OneTimeEvent)
                GetComponent<Collider>().enabled = false;
        }
    }

}
