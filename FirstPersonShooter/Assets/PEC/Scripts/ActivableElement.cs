﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActivableElement : MonoBehaviour {

    public abstract void Plug();
    public abstract bool IsPlugged();

    public abstract void Activate();
    public abstract bool IsActivated();

}
