﻿using System.Collections;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PECController : Actor {

    [SerializeField]
    private bool IsInmortal;

    [SerializeField] private float ChangeWepTime;
    [SerializeField] private Weapon[] Weps;
    [SerializeField] private float Shield;

    [SerializeField] private AudioSource ReloadWepSound;

    public bool IsDead { get; private set; }
    public bool CanMove { get => fpsController.CanMove; set => fpsController.CanMove = value; }

    private FirstPersonController fpsController;
    private CharacterController chController;
    private ButtonController button;
    private int actualWeaponIdx;
    private int keys;
    private bool isChangingWep, isReloading;
    private float height;
    private Transform tr;

    private float startFOV, startAgentHeight;
    private bool isTargeting;


    private void Awake() {
        IsDead = isChangingWep = false;
        tr = transform;
        chController = GetComponent<CharacterController>();
        fpsController = GetComponent<FirstPersonController>();
        CanMove = IsPlayer = true;

        height = chController.height / 6;

        startFOV = Camera.main.fieldOfView;
        startAgentHeight = chController.height;
    }

    private void Start() {
        actualWeaponIdx = 0;
        foreach (Weapon w in Weps)
            w.gameObject.SetActive(false);
        Weps[actualWeaponIdx].gameObject.SetActive(true);
        Weps[actualWeaponIdx].PlayReadySound();
        UpdateUI();
    }

    public void ResetPlayer(Transform t) {
        tr.SetPositionAndRotation(t.position, t.rotation);
        Life = 75f;
        Shield = 50f;
        foreach (Weapon w in Weps) {
            if (w is FireWeaponController) {
                FireWeaponController fw = w as FireWeaponController;
                if (fw.GetAmount < fw.BulletsPerCartridge)
                    fw.AddAmmo(fw.BulletsPerCartridge);
            }
        }
        GameManager.UpdateAmmoText(Weps[actualWeaponIdx].GetLoadedBullets, Weps[actualWeaponIdx].GetCartridgesBullets);
        GameManager.UpdateLifeShield(Life, Shield);
    }
    public void EnableInput(bool e) {
        IsDead = !e;
        CanMove = e;
    }

	
    private void Update() {
        if (IsDead || !CanMove)
            return;
		//interactuar
        if (Input.GetKeyDown(KeyCode.E)) {
            if (button) {
                button.Activate();
                GameManager.ShowEmpty();
            }
        }
		//recargar
        if (!isChangingWep && Input.GetKeyDown(KeyCode.R) && !(Weps[actualWeaponIdx] is ThrowableWeaponController)) {
            if ((Weps[actualWeaponIdx] as FireWeaponController).CanReload()) {
                isReloading = true;
                if (ReloadWepSound)
                    ReloadWepSound.Play();
                Debug.Log(ReloadWepSound.clip.name);
                //Invoke("ReloadWep", ReloadWepSound.clip.length * 0.5f);
                Invoke("ReloadWep", 2f);
            }
        }
		//agacharse
        if (Input.GetKeyDown(KeyCode.LeftControl)) {
            chController.height = startAgentHeight / 2;
            tr.position = new Vector3(tr.position.x, tr.position.y - startAgentHeight / 4, tr.position.z);
        } else if (Input.GetKeyUp(KeyCode.LeftControl)) {
            chController.height = startAgentHeight;
        }
        //disparar
        if (!isReloading && !isChangingWep && Input.GetMouseButtonDown(0)) {
            Weps[actualWeaponIdx].ButtonDown();
        } else if (!isReloading && !isChangingWep && Input.GetMouseButton(0)) {
            Weps[actualWeaponIdx].ButtonHeld();
        } else if (Input.GetMouseButtonUp(0)) {
            Weps[actualWeaponIdx].ButtonUp();
        }
        //cambiar el arma
        if (!isTargeting && !isChangingWep && Input.GetAxis("Mouse ScrollWheel") != 0) {
            if (isReloading) {
                if (ReloadWepSound)
                    ReloadWepSound.Stop();
                CancelInvoke("ReloadWep");
                isReloading = false;
            }
            isChangingWep = true;
            Transform actualWep = Weps[actualWeaponIdx].transform;
            actualWeaponIdx -= (int) Mathf.Sign(Input.GetAxis("Mouse ScrollWheel"));
            //actualWeaponIdx = Mathf.Max(0, Mathf.Min(Weps.Length - 1, actualWeaponIdx));
            if (actualWeaponIdx == -1) actualWeaponIdx = Weps.Length - 1;
            else if (actualWeaponIdx == Weps.Length) actualWeaponIdx = 0;
            Debug.Log(actualWeaponIdx);
            StartCoroutine(ChangeWeaponEffect(actualWep, Weps[actualWeaponIdx].transform));
            UpdateUI();
        }        
        //apuntar (rifle)
        if (Input.GetMouseButtonDown(1)) {
            if (Weps[actualWeaponIdx].GetAmmoType != GameManager.AmmoType.Rifle)
                return;
            isTargeting = true;
            Camera.main.fieldOfView = startFOV / 2;
        } else if (Input.GetMouseButtonUp(1)) {
            isTargeting = false;
            Camera.main.fieldOfView = startFOV;
        }
    }


    private void ReloadWep() {
        Weps[actualWeaponIdx].Reload();
        isReloading = false;
        ReloadWepSound.Stop();
        Weps[actualWeaponIdx].PlayReadySound();
        UpdateUI();
    }


    public bool UseKey() {
        if (keys <= 0)
            return false;
        keys--;
        GameManager.UpdateKeys(keys);
        return true;
    }


    public override void TakeDamage(float dmg) {
        if (IsDead || IsInmortal)
            return;
        GameManager.RedPanelFlash();
        if (Shield > 0) {
            Shield -= dmg;
            Shield = Mathf.Max(0, Shield);
            dmg *= 0.2f;
        }
        if (HitSounds != null && HitSounds.Length > 0)
            HitSounds[Random.Range(0, HitSounds.Length)].Play();
        Life -= dmg;
        Life = Mathf.Max(0, Life);
        GameManager.UpdateLifeShield(Life, Shield);
        if (Life <= 0) {
            EnableInput(false);
            if (DieSound)
                DieSound.Play();
            GameManager.GameOver();
        }
    }


	//llamado desde el arma para avisar al jugador, solo para actualizar la UI
    public override void Fire(float amount) {
        UpdateUI();
    }
    public override void AddAmmo(float amount) {
        UpdateUI();
    }
	
	
    private void AddLife(float amount) {
        Life += amount;
        Life = Mathf.Min(100, Life);
        GameManager.UpdateLifeShield(Life, Shield);
    }
    private void AddShield(float amount) {
        Shield += amount;
        Shield = Mathf.Min(100, Shield);
        GameManager.UpdateLifeShield(Life, Shield);
    }
    private void AddKey(int amount) {
        keys += amount;
        GameManager.UpdateKeys(keys);
    }


    private void UpdateUI() {
        Weapon w = Weps[actualWeaponIdx];
        GameManager.UpdateAmmoImage(w.GetAmmoType);
        GameManager.UpdateAmmoText(w.GetLoadedBullets, w.GetCartridgesBullets);
    }


    public Weapon GetWeaponByType(GameManager.AmmoType type) {
        foreach (Weapon w in Weps)
            if (w.GetAmmoType == type)
                return w;
        return null;
    }


    public bool IsMoving() => fpsController.Speed > 1;


    
	//efecto de cambiar de arma: consiste en bajar el arma actual hasta
	//que ya no salga por pantalla para entonces desactivarla, activar la siguiente
	//y subirla hasta la posicion del arma anterior.
    private IEnumerator ChangeWeaponEffect(Transform actualWep, Transform nextWep) {
        float progress = 0;
        Vector3 upPos = actualWep.localPosition, downPos = actualWep.localPosition + Vector3.down * height;
        while (progress <= 1) {
            progress += Time.deltaTime / ChangeWepTime;
            actualWep.localPosition = Vector3.Lerp(upPos, downPos, progress);
            yield return null;
        }
        actualWep.gameObject.SetActive(false);
        nextWep.localPosition = downPos;
        nextWep.gameObject.SetActive(true);
        progress = 0;
        while (progress <= 1) {
            progress += Time.deltaTime / ChangeWepTime;
            nextWep.localPosition = Vector3.Lerp(downPos, upPos, progress);
            yield return null;
        }
        isChangingWep = false;
        Weps[actualWeaponIdx].PlayReadySound();
    }


    private void OnTriggerEnter(Collider other) {
        int layer = other.gameObject.layer;

        //botones
        if (GameManager.CheckButtonMask(layer)) {
            button = other.GetComponentInParent<ButtonController>();
            if (!button.IsPlugged()) {
                GameManager.ShowNotPluggedInfo();
            } else if (!button.IsActivated()) {
                GameManager.ShowPressEInfo();
            } else {
                GameManager.ShowEmpty();
            }

        //deadzone
        } else if (GameManager.CheckDeadMask(layer)) {
            if (IsDead)
                return;
            if (DieSound)
                DieSound.Play();
            EnableInput(false);
            GameManager.GameOver();

        //items
        } else if (GameManager.CheckItemsMask(layer)) {
            Item item = other.GetComponent<Item>();
            switch (item.Type) {
                case Item.ItemType.AmmoRifle:
                    Weapon r = GetWeaponByType(GameManager.AmmoType.Rifle);
                    r.AddAmmo(item.Amount);
                    break;
                case Item.ItemType.AmmoPistol:
                    Weapon p = GetWeaponByType(GameManager.AmmoType.Pistol);
                    p.AddAmmo(item.Amount);
                    break;
                case Item.ItemType.AmmoGrenade:
                    Weapon g = GetWeaponByType(GameManager.AmmoType.Grenade);
                    g.AddAmmo(item.Amount);
                    break;
                case Item.ItemType.Life:
                    AddLife(item.Amount);
                    break;
                case Item.ItemType.Shield:
                    AddShield(item.Amount);
                    break;
                case Item.ItemType.Key:
                    AddKey(item.Amount);
                    break;
            }
            item.Grab();
            UpdateUI();
        }
    }


    private void OnTriggerExit(Collider other) {
        int layer = other.gameObject.layer;

        //para quitar el mensaje de pantalla al no estar en contacto con botones o puertas
        if (GameManager.CheckButtonMask(layer) || GameManager.CheckDoorMask(layer)) {
            button = null;
            GameManager.ShowEmpty();
        }
    }
    
}
