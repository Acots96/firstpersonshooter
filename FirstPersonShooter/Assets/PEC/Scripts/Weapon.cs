﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour {

    public abstract void ButtonDown();
    public abstract void ButtonHeld();
    public abstract void ButtonUp();

    [SerializeField]
    protected float Amount, Damage;
    protected float loadedBullets, cartridgesBullets;
    [SerializeField]
    protected GameManager.AmmoType AmmoType;

    public abstract void AddAmmo(int amount);

    public float GetAmount { get => Amount; }
    public float GetDamage { get => Damage; }
    public float GetLoadedBullets { get => loadedBullets; }
    public float GetCartridgesBullets { get => cartridgesBullets; }
    public GameManager.AmmoType GetAmmoType { get => AmmoType; }

    public virtual void Reload() {}

    protected Actor owner;

    [SerializeField]
    protected AudioSource WeaponReadySound;

    public void PlayReadySound() {
        if (WeaponReadySound && Amount > 0)
            WeaponReadySound.Play();
    }

    public void Start() {
        owner = GetComponentInParent<Actor>();
    }
}
