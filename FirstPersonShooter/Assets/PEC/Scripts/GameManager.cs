﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : ActivableElement {

    public enum AmmoType { Rifle, Pistol, Grenade }

    [SerializeField] private Text HelpText;
    [SerializeField] private string NotPluggedInfo, PressEInfo, GameOverInfo, PressAnyKeyInfo, NeedKeyInfo, WinInfo, PressAnyKeyRestartInfo;
    [SerializeField] private Text LifeText, ShieldText, AmmoText, KeysText;
    [SerializeField] private GameObject RifleImage, PistolImage, GrenadeImage, RifleTargetIcon, PistoltargetIcon, GrenadeTargetIcon;

    [SerializeField] private Image Panel, RedPanel;
    [SerializeField] private float FadeTime;

    [SerializeField] private LayerMask ButtonMask, DeadMask, PlayerMask, ItemsMask, 
        ActorsMask, ActorHeadMask, WeaponIgnoreMask, DoorMask, WatchPlayerMask;

    [Header("Info texts")]
    [SerializeField] private Image InfoPanel;
    [SerializeField] private Text InfoText;
    [SerializeField] private string[] InfoStrings;

    private PECController controller;
    public static PECController Player { get => Instance.controller; }

    [SerializeField] private Transform[] CheckPoints;
    private int actualCheckPoint;


    private static GameManager Instance;

    public static bool IsDead { get => Instance.controller.IsDead; }


    private void Awake() {
        if (Instance) {
            Destroy(Instance);
            Instance = null;
        }
        Instance = this;
        actualCheckPoint = 0;

        controller = FindObjectOfType<PECController>();
    }


    public static bool CheckPlayerHasKey() => Instance.controller.UseKey();

    public static Vector3 GetPlayerPos() => Instance.controller.transform.position;


    public static bool CheckButtonMask(int layer) => CheckMask(layer, Instance.ButtonMask);
    public static bool CheckDoorMask(int layer) => CheckMask(layer, Instance.DoorMask);
    public static bool CheckDeadMask(int layer) => CheckMask(layer, Instance.DeadMask);
    public static bool CheckPlayerMask(int layer) => CheckMask(layer, Instance.PlayerMask);
    public static bool CheckItemsMask(int layer) => CheckMask(layer, Instance.ItemsMask);
    public static bool CheckActorHeadMask(int layer) => CheckMask(layer, Instance.ActorHeadMask);

    public static bool CheckMask(int layer, LayerMask mask) => mask.value == (mask.value | (1 << layer));

    public static int GetActorsMask() => Instance.ActorsMask.value;
    public static int GetPlayerMask() => Instance.PlayerMask.value;
    public static int GetWeaponIgnoreMask() => Instance.WeaponIgnoreMask.value;
    public static int GetWatchPlayerLayer() => Instance.WatchPlayerMask.value;


    public static void GameOver() {
        Instance.StartCoroutine(Instance.GameOverEffect());
    }

    //corutina de GameOver que hace el fade a negro,
    //comunica al jugador lo que debe hacer (pulsar una tecla),
    //resetea el jugador y hace el fade hacia transparente.
    private IEnumerator GameOverEffect() {
        float progress = 0;
        while (progress <= 1) {
            progress += Time.deltaTime / FadeTime;
            Color c = Panel.color;
            c.a = progress;
            Panel.color = c;
            yield return null;
        }
        HelpText.text = GameOverInfo;
        HelpText.text += "\n\n\n";
        yield return new WaitForSeconds(FadeTime / 2f);
        HelpText.text += PressAnyKeyInfo;
        //
        yield return new WaitUntil(() => Input.anyKeyDown);
        HelpText.text = "";
        foreach (PlatformController p in FindObjectsOfType<PlatformController>())
            p.ResetPlatform();
        controller.ResetPlayer(CheckPoints[actualCheckPoint]);
        //
        while (progress > 0) {
            progress -= Time.deltaTime / (FadeTime * 0.5f);
            Color c = Panel.color;
            c.a = progress;
            Panel.color = c;
            yield return null;
        }
        controller.EnableInput(true);
    }


    public static void ShowNotPluggedInfo() {
        Instance.HelpText.text = Instance.NotPluggedInfo;
    }

    public static void ShowNeedKeyInfo() {
        Instance.HelpText.text = Instance.NeedKeyInfo;
    }

    public static void ShowPressEInfo() {
        Instance.HelpText.text = Instance.PressEInfo;
    }

    public static void ShowEmpty() {
        Instance.HelpText.text = string.Empty;
    }


    //muestra el texto con barra semitransparente que indica
    //al jugador el objetivo de la zona a la que ha llegado
    public void ShowGameInfoPart(int part) {
        StartCoroutine(InfoTextEffect(InfoStrings[part]));
    }
    private IEnumerator InfoTextEffect(string t) {
        float progress = 0, fadeTime = 0.5f;
        InfoText.text = t;
        while (progress <= 1) {
            progress = progress += Time.deltaTime / (fadeTime * 2f);
            Color c = InfoPanel.color;
            c.a = progress * 0.75f;
            InfoPanel.color = c;
            c = InfoText.color;
            c.a = progress;
            InfoText.color = c;
            yield return null;
        }
        yield return new WaitForSeconds(5f);
        while (progress >= 0) {
            progress = progress -= Time.deltaTime / fadeTime;
            Color c = InfoPanel.color;
            c.a = progress * 0.75f;
            InfoPanel.color = c;
            c = InfoText.color;
            c.a = progress;
            InfoText.color = c;
            yield return null;
        }
    }


    //actualiza la informacion del arma que tiene el jugador en ese momento
    public static void UpdateAmmoText(float loadedBullets, float cartridgesBullets) {
        Instance.AmmoText.text = loadedBullets + "/" + cartridgesBullets;
    }
    public static void UpdateAmmoImage(AmmoType type) {
        bool rifle = type == AmmoType.Rifle,
            pistol = type == AmmoType.Pistol,
            grenade = type == AmmoType.Grenade;
        Instance.RifleImage.SetActive(rifle); Instance.RifleTargetIcon.SetActive(rifle);
        Instance.PistolImage.SetActive(pistol); Instance.PistoltargetIcon.SetActive(pistol);
        Instance.GrenadeImage.SetActive(grenade); Instance.GrenadeTargetIcon.SetActive(grenade);
    }

    public static void UpdateLifeShield(float life, float shield) {
        Instance.LifeText.text = life + "";
        Instance.ShieldText.text = shield + "";
    }

    public static void UpdateKeys(float keys) {
        Instance.KeysText.text = keys + "";
    }


    public static void RedPanelFlash() {
        Instance.RedPanel.gameObject.SetActive(true);
        Instance.Invoke("RedPanelFlashOff", 0.1f);
    }

    private void RedPanelFlashOff() {
        Instance.RedPanel.gameObject.SetActive(false);
    }


    public void NewCheckPoint() {
        actualCheckPoint++;
    }



    [SerializeField] private AudioSource WinSound;

    public override bool IsPlugged() => true;
    public override bool IsActivated() => false;

    public override void Plug() {}
    public override void Activate() {
        StartCoroutine(WinEffect());
    }

    //corutina con funcionamiento parecido a la de GameOver
    //con la diferencia de que recarga la escena para empezar de nuevo
    private IEnumerator WinEffect() {
        controller.EnableInput(false);
        //
        if (WinSound)
            WinSound.Play();
        float progress = 0;
        while (progress <= 1) {
            progress += Time.deltaTime / FadeTime;
            Color c = Panel.color;
            c.a = progress;
            Panel.color = c;
            yield return null;
        }
        HelpText.text = WinInfo;
        HelpText.text += "\n\n\n";
        HelpText.text += PressAnyKeyRestartInfo;
        //
        yield return new WaitUntil(() => Input.anyKeyDown);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
