﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : Actor {
    
    [SerializeField] private MeshRenderer[] Meshes;
    [SerializeField] private float DieFadeTime;

    [SerializeField] private FireWeaponController wep;

    [SerializeField] private Transform WanderPointsParent, Head;
    [SerializeField] private float WatchDistThreshold, WatchAngle, ListenDistThreshold;

    [SerializeField] private GameObject ItemToDropPrefab;

    private Transform tr;
    private List<Material> mats;
    private bool isDead;
    private NavMeshAgent agent;
    private float normalSpeed, fastSpeed, normalTurnSpeed, fastTurnSpeed;

    private List<Vector3> localWatchDirections;
    private int actualWatchAngle;

    private bool playerWatched, isShooting, hasListenedPlayer, hasTakenDamage, contactCountdownStarted, chasePlayer;

    private List<Vector3> wanderPoints;


    private void Awake() {
        tr = transform;
        IsPlayer = false;
        if (Head == null)
            Head = tr;
        foreach (MeshRenderer mesh in Meshes)
            mesh.materials[0] = new Material(mesh.materials[0]);
        mats = new List<Material>();
        foreach (MeshRenderer mesh in Meshes)
            mats.Add(mesh.materials[0]);

        //guarda los vectores de forma local, para que siendo transform.forward el central,
        //los vectores vayan desde -WatchAngle/2 hasta WatchAngle/2
        localWatchDirections = new List<Vector3>();
        float startLocalAngle = -WatchAngle / 2, angleIncr = WatchAngle / 10;
        for (float angle = startLocalAngle; angle <= -startLocalAngle; angle += angleIncr) {
            Vector3 localAngle = new Vector3(Mathf.Sin(angle * Mathf.Deg2Rad), 0, Mathf.Cos(angle * Mathf.Deg2Rad));
            localWatchDirections.Add(localAngle);
        }
        actualWatchAngle = 0;
        WatchDistThreshold *= tr.localScale.x;
        ListenDistThreshold *= tr.localScale.x;

        agent = GetComponent<NavMeshAgent>();
        wanderPoints = new List<Vector3>();
        foreach (Transform p in WanderPointsParent)
            wanderPoints.Add(p.position);
        normalSpeed = agent.speed;
        fastSpeed = agent.speed * 2;
        normalTurnSpeed = agent.angularSpeed;
        fastTurnSpeed = agent.angularSpeed * 2;
        StartCoroutine(WanderEffect());
    }

    private void Update() {
        if (isDead)
            return;

        if (GameManager.Player.IsDead) {
            playerWatched = hasListenedPlayer = hasTakenDamage = chasePlayer = contactCountdownStarted = false;
            CancelInvoke("ContactCountdown");
            return;
        }

        //si el player esta fuera de un cierto rango tras haber sido visto
        //deja de verlo y lo busca (vuelta sobre si mismo)
        float dist = Vector3.Distance(tr.position, GameManager.GetPlayerPos());
        if (dist > WatchDistThreshold * 2f) {
            if (playerWatched) {
                playerWatched = false;
                //chasePlayer = false;
                wep.ButtonUp();
                StartCoroutine(LookingAroundAfterListen());
            }
            return;
        }

        //si de repente deja de ver al player inicia una cuenta atras
        //hasta que le vuelve a ver. Si no lo ve al finalizar el tiempo
        //entonces lo olvida
        if (playerWatched && !contactCountdownStarted) {
            if (!IsWatchingPlayer()) {
                agent.SetDestination(GameManager.Player.transform.position);
                playerWatched = false;
                contactCountdownStarted = true;
                Invoke("ContactCountdown", 10f);
            }
        }
        //si no lo ha visto lo busca
        if (!playerWatched) {
            playerWatched = WatchForPlayer();
        }
        //si lo ha visto o ha recibido un disparo suyo entonces lo persigue
        if (playerWatched || hasTakenDamage) {
            if (contactCountdownStarted) {
                contactCountdownStarted = false;
                CancelInvoke("ContactCountdown");
            }
            chasePlayer = true;
            //tr.LookAt(GameManager.Player.transform);
            Vector3 toLookAt = GameManager.Player.transform.position;
            toLookAt.y = tr.position.y;
            tr.LookAt(toLookAt);
            if (!isShooting) {
                isShooting = true;
                StartCoroutine(wep.IsAutomatic ? AutomaticShoot() : NotAutomaticShoot());
            }
            return;
        } else {
            tr.LookAt(null);
        }

        //si no lo ha oido pero esta dentro del rango de oido y el jugador se
        //esta moviendo entonces da una vuelta sobre si mismo para buscarlo
        if (!hasListenedPlayer && dist <= ListenDistThreshold) {
            if (GameManager.Player.IsMoving()) {
                hasListenedPlayer = true;
                StartCoroutine(LookingAroundAfterListen());
                return;
            }
        }
    }

    private void ContactCountdown() {
        playerWatched = hasListenedPlayer = hasTakenDamage = chasePlayer = contactCountdownStarted = false;
        wep.ButtonUp();
    }


    //comprueba si tiene delante al jugador con un spherecast, 
    //que hace la misma funcion que el raycast pero mas amplio
    //para poder detectarlo mas facilmente
    private bool IsWatchingPlayer() {
        Ray r = new Ray(Head.position, tr.forward);
        if (Physics.SphereCast(r, 5f, out RaycastHit hit, WatchDistThreshold * 2f, GameManager.GetWatchPlayerLayer(), QueryTriggerInteraction.Ignore))
            return hit.transform.GetComponent<PECController>() != null;
        return false;
    }

    //busca al player mediante spherecast tambien, pero solo con un vector por frame
    //para evitar un gasto de recursos innecesario
    private bool WatchForPlayer() {
        Vector3 origin = Head.position;
        Vector3 direction = tr.TransformDirection(localWatchDirections[actualWatchAngle]);
        actualWatchAngle++;
        if (actualWatchAngle == localWatchDirections.Count)
            actualWatchAngle = 0;
        Ray r = new Ray(origin, direction);
        bool b = Physics.SphereCast(r, 5f, out RaycastHit hit, WatchDistThreshold * 2f, GameManager.GetWatchPlayerLayer(), QueryTriggerInteraction.Ignore);
        if (b)
            return hit.transform.GetComponent<PECController>() != null;
        return false;
    }


    //corutinas para disparar, siempre con valores aleatorios
    //para mas veracidad
    private IEnumerator AutomaticShoot() {
        yield return new WaitForSeconds(Random.value * 0.5f + 1);
        int bullets = (int) (Random.value * 4f + 1f);
        wep.ButtonDown();
        while (bullets-- > 0) {
            wep.ButtonHeld();
            yield return null;
        }
        yield return new WaitForSeconds(0.1f);
        wep.ButtonUp();
        //yield return new WaitForSeconds(Random.value * 0.5f + 1);
        isShooting = false;
    }
    private IEnumerator NotAutomaticShoot() {
        yield return new WaitForSeconds(Random.value + 1);
        wep.ButtonDown();
        yield return new WaitForSeconds(0.1f);
        wep.ButtonUp();
        //yield return new WaitForSeconds(Random.value + 1);
        isShooting = false;
    }

    //corutina para que el enemigo de una vuelta sobre si mismo
    //para buscar al jugador, dado un tiempo de vuelta.
    //si lo ve deja de girar
    private IEnumerator LookingAroundAfterListen() {
        float progress = 0, lookAroundTime = 4f;
        float rads = 360f / lookAroundTime;
        while (progress <= 1 && !playerWatched) {
            progress += Time.deltaTime / lookAroundTime;
            tr.Rotate(Vector3.up * rads * Time.deltaTime);
            yield return null;
        }
        hasListenedPlayer = false;
        wep.ButtonUp();
    }


    public override void TakeDamage(float dmg) {
        if (isDead)
            return;
        Life -= dmg;
        hasTakenDamage = chasePlayer = true;
        if (HitSounds != null && HitSounds.Length > 0) {
            AudioSource a = HitSounds[Random.Range(0, HitSounds.Length)];
            a.Play();
        }
        if (Life <= 0) {
            isDead = true;
            if (DieSound)
                DieSound.Play();
            //simulacion de golpe mortal y empujon
            wep.ButtonUp();
            wep.enabled = false;
            Rigidbody rb = wep.gameObject.AddComponent<Rigidbody>();
            rb.AddTorque(new Vector3(0, 0, 2 * Random.Range(-1f, 1f)), ForceMode.Impulse);
            wep.transform.parent = null;
            Bounds b = tr.GetChild(0).GetComponent<Collider>().bounds;
            GetComponent<Rigidbody>().freezeRotation = false;
            GetComponent<Rigidbody>().AddForceAtPosition(new Vector3(3, 0, 3), b.max, ForceMode.Impulse);
            //drop del item
            if (ItemToDropPrefab)
                Instantiate(ItemToDropPrefab, tr.position, ItemToDropPrefab.transform.rotation);
            //fade
            StartCoroutine(DieFade());
        }
    }

    //corutina que hace un fade mediante el color del material
    //sobre todos los MeshRenderers
    private IEnumerator DieFade() {
        float progress = 1;
        while (progress >= 0) {
            progress -= Time.deltaTime / DieFadeTime;
            foreach (Material mat in mats) {
                Color c = mat.color;
                c.a = progress;
                mat.color = c;
            }
            yield return null;
        }
        Destroy(gameObject);
    }

    
    //corutina que controla el movimiento de wander y espera mientras esta
    //viendo al jugador, oyendole o disparando
    private IEnumerator WanderEffect() {
        while (!isDead) {
            //wander
            Vector3 destPos = wanderPoints[Random.Range(0, wanderPoints.Count)];
            agent.SetDestination(destPos);
            yield return new WaitWhile(() => !playerWatched && !hasListenedPlayer && !hasTakenDamage && !chasePlayer
                && !isDead && Vector3.Distance(destPos, tr.position) > agent.stoppingDistance);

            //ve al jugador o recibe un tiro suyo
            if (playerWatched || hasTakenDamage) {
                Vector3 p = GameManager.GetPlayerPos();
                agent.SetDestination(p);
                agent.speed = fastSpeed;
                agent.angularSpeed = fastTurnSpeed;
                WaitForSeconds wfs = new WaitForSeconds(0.5f);
                
                //espera mientras siga vivo y lo persiga
                while (!isDead && chasePlayer) {
                    p = GameManager.GetPlayerPos();
                    float dist = Vector3.Distance(p, tr.position);
                    //para a cierta distancia del objetivo para no ponerse encima suyo
                    if (dist > 40f) {
                        agent.SetDestination(p);
                    } else {
                        agent.SetDestination(tr.position);
                    }
                    yield return wfs;
                }
                wep.ButtonUp();

            //espera mientras lo haya oido
            } else if (hasListenedPlayer) {
                agent.isStopped = true;
                agent.angularSpeed = fastTurnSpeed;
                yield return new WaitWhile(() => hasListenedPlayer);
            }

            agent.speed = normalSpeed;
            agent.angularSpeed = normalTurnSpeed;
            agent.isStopped = false;
        }

        agent.SetDestination(tr.position);
        agent.isStopped = true;
    }

}
